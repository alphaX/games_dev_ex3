/*
 * Mesh.cpp
 *
 *  Created on: Apr 13, 2013
 *      Author: alex
 */

#include "Mesh.hpp"

Mesh::Mesh(GLfloat * vertices, int numberOfVertices, GLuint * indexes, int numberOfIndexes) {
	// TODO Auto-generated constructor stub
	this->vertices = vertices;
	this->indexes = indexes;
	this->verticesSize = numberOfVertices;
	this->indexesSize = numberOfIndexes;
}

Mesh::~Mesh() {
	// TODO Auto-generated destructor stub
}

GLuint* Mesh::getIndexes() {
	return indexes;
}

int Mesh::getIndexesSize() {
	return indexesSize;
}

int Mesh::getVerticesSize() {
	return this->verticesSize;
}

GLfloat* Mesh::getVertices() {
	return vertices;
}

