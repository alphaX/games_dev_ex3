/*
 * GLDrawer.h
 *
 *  Created on: Apr 13, 2013
 *      Author: alex
 */

#ifndef GLDRAWER_H_
#define GLDRAWER_H_

#include <GL/glew.h>
#include <GL/glfw.h>

#include <Cg/cg.h>
#include <Cg/cgGL.h>

#include "Mesh.hpp"

class GLDrawer {
	Mesh * mesh;
	GLuint vertexBuffer;
	GLuint indexBuffer;
	CGprogram cgProgram;

	CGparameter positionParameter;

public:
	GLDrawer(Mesh * mesh, CGprogram program);
	void draw();
	virtual ~GLDrawer();
};

#endif
