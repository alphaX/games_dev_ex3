#include <stdio.h>
#include <stdlib.h>

#include <vector>

#include <GL/glew.h>
#include <GL/glfw.h>

// Cg headers
#include <Cg/cg.h>
#include <Cg/cgGL.h>

// GLM headers
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp> // for glm::value_ptr

#include "player.hpp"
#include "camera.hpp"

#include "Mesh.hpp"
#include "GLDrawer.hpp"

using namespace glm;

const int winHeight = 768, winWidth = 1024;

CGprogram cgRenderVP;

CGcontext		cgContext;
CGeffect 		cgEffect;
CGtechnique		cgTechnique;
CGpass 			cgRenderPass, cgPostProcessPass, cgMotionBlurPass;

CGparameter		shaderWVP, shaderTexture, shaderSampler;
CGparameter 	cgRenderPos, cgPostProcessPos;
CGparameter		shaderBlurKernel, shaderScreenSize;
CGparameter		shaderDepthTexture, shaderDepthSampler, shaderpWvp, shaderiWvp, shaderMotionBlurTrailLength;

GLuint vertexbuffer, tex, depthTex, fb, depth_rb, rectbuffer;

mat4 mProjection = perspective(45.0f,(float)winWidth / winHeight, 0.1f, 100.f);
mat4 mView = translate( mat4(1.0f), vec3(0.0f, 0.0f, -10.0f));
mat4 mWorld;
mat4 wvp;
mat4 pWvp;

int previouseMouseX, previouseMouseY;
bool firstFrame = true;
bool motionBlurOn = false;

const char winTitle[] = "Game dev Ex 3";

glm::mat4 identitylKernel(
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f
);

glm::mat4 gaussianKernel(
		0.0f/16, 1.0f/16, 1.0f/16, 0.0f/16,
		1.0f/16, 2.0f/16, 2.0f/16, 1.0f/16,
		1.0f/16, 2.0f/16, 2.0f/16, 1.0f/16,
		0.0f/16, 1.0f/16, 1.0f/16, 0.0f/16
);

void setupBlurKernel(glm::mat4 kernel){
	cgGLSetMatrixParameterfc(shaderBlurKernel, value_ptr(kernel));
}

void setupMotionBlurTrailLength(float length){
	cgGLSetParameter1f(shaderMotionBlurTrailLength, length);
}

void initGL(const char * title) {
	if( !glfwInit() ) {
		fprintf( stderr, "Failed to initialise GLFW\n" );
		exit(EXIT_FAILURE);
	}

	glfwOpenWindowHint(GLFW_FSAA_SAMPLES, 4);
	glfwOpenWindowHint(GLFW_OPENGL_VERSION_MAJOR, 3);
	glfwOpenWindowHint(GLFW_OPENGL_VERSION_MINOR, 3);
	glfwOpenWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);
	glfwOpenWindowHint(GLFW_OPENGL_FORWARD_COMPAT,GL_FALSE);

	// Open a window and create its OpenGL context
	if( !glfwOpenWindow( winWidth, winHeight, 0,0,0,0, 32,0, GLFW_FULLSCREEN ) ) {
		fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible.\n" );
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	// Initialise GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialise GLEW\n");
		exit(EXIT_FAILURE);
	}

	glfwSetWindowTitle( title);

	// Ensure we can capture the escape key being pressed below
	glfwEnable( GLFW_STICKY_KEYS );

	glClearColor(0.1f, 0.15f, 0.1f, 0.0f);
}

void initCg (const char * fx_file_path) {

	if(!(cgContext = cgCreateContext())) {
		fprintf(stderr, "Failed to create Cg Context\n");
		exit(EXIT_FAILURE);
	}

	cgGLRegisterStates(cgContext);

	// compile the effect from file
	if(!(cgEffect = cgCreateEffectFromFile(cgContext, fx_file_path, NULL))) {
		fprintf(stderr, "Failed to load effect file: %s\n\n%s\n", fx_file_path,  cgGetLastListing(cgContext));
		exit(EXIT_FAILURE);
	}

	for(cgTechnique = cgGetFirstTechnique(cgEffect);
			cgTechnique && cgValidateTechnique(cgTechnique) == CG_FALSE;
			cgTechnique = cgGetNextTechnique(cgTechnique));

	if (cgTechnique) {
		printf("Using technique %s.\n", cgGetTechniqueName(cgTechnique));
	} else {
		fprintf(stderr, "No valid technique found in %s\n", fx_file_path);
		exit(EXIT_FAILURE);
	}

	cgRenderPass = cgGetFirstPass(cgTechnique),
	cgPostProcessPass = cgGetNextPass(cgRenderPass);
	cgMotionBlurPass = cgGetNextPass(cgPostProcessPass);
	CGprogram cgPostProcessVP;
	cgRenderVP = cgGetPassProgram(cgRenderPass, CG_VERTEX_DOMAIN);
	cgPostProcessVP = cgGetPassProgram(cgPostProcessPass, CG_VERTEX_DOMAIN);

	// Get handles to parameters in the shader so that we can access them from CPU-side code
	if( !(shaderWVP = cgGetNamedEffectParameter(cgEffect, "gWvpXf"))){
		fprintf(stderr, "Cannot get a handle to a shader parameter gWvpXf\n");
		exit(EXIT_FAILURE);
	}

	if(!(shaderSampler = cgGetNamedEffectParameter(cgEffect, "sam"))){
		fprintf(stderr, "Cannot get a handle to a shader parameter sam\n");
		exit(EXIT_FAILURE);
	}

	if(!(shaderDepthSampler = cgGetNamedEffectParameter(cgEffect, "depthSam"))){
		fprintf(stderr, "Cannot get a handle to a shader parameter death sam\n");
		exit(EXIT_FAILURE);
	}

	if(!(shaderTexture = cgGetNamedEffectParameter(cgEffect, "tex"))){
		fprintf(stderr, "Cannot get a handle to a shader parameter tex\n");
		exit(EXIT_FAILURE);
	}

	if(!(shaderDepthTexture = cgGetNamedEffectParameter(cgEffect, "depthTex"))){
		fprintf(stderr, "Cannot get a handle to a shader parameter depth tex\n");
		exit(EXIT_FAILURE);
	}

	if(!(cgPostProcessPos = cgGetNamedParameter(cgPostProcessVP, "pos"))) {
		fprintf(stderr, "Cannot get a handle to a PostProcesing VP parameter pos\n");
		exit(EXIT_FAILURE);
	}

	if( !(shaderBlurKernel = cgGetNamedEffectParameter(cgEffect, "blurKernel"))){
		fprintf(stderr, "Cannot get a handle to a shader parameter blurKernel\n");
		exit(EXIT_FAILURE);
	}

	if( !(shaderScreenSize = cgGetNamedEffectParameter(cgEffect, "screenSize"))){
		fprintf(stderr, "Cannot get a handle to a shader parameter screenSize\n");
		exit(EXIT_FAILURE);
	}

	if( !(shaderpWvp = cgGetNamedEffectParameter(cgEffect, "pWvp"))){
		fprintf(stderr, "Cannot get a handle to a shader parameter pWvp\n");
		exit(EXIT_FAILURE);
	}

	if( !(shaderiWvp = cgGetNamedEffectParameter(cgEffect, "iWvp"))){
		fprintf(stderr, "Cannot get a handle to a shader parameter iWvp\n");
		exit(EXIT_FAILURE);
	}

	if( !(shaderMotionBlurTrailLength = cgGetNamedEffectParameter(cgEffect, "motionBlurTrailLength"))){
		fprintf(stderr, "Cannot get a handle to a shader parameter motionBlurTrailLength\n");
		exit(EXIT_FAILURE);
	}

}

void initFramebuffer(){
	glGenFramebuffers(1, &fb);
	glBindFramebuffer(GL_FRAMEBUFFER, fb);

	// RGBA8 2D texture, 24 bit depth texture, 256x256
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);

	GLFWvidmode return_struct;

	glfwGetDesktopMode( &return_struct );
	int bufferWidth = return_struct.Width, bufferHeight = return_struct.Height;
	cgGLSetParameter2f(shaderScreenSize, bufferWidth, bufferHeight);
	printf("window size: %d, %d", bufferWidth, bufferHeight);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, bufferWidth, bufferHeight, 0, GL_BGRA, GL_UNSIGNED_BYTE, NULL); //NULL means reserve texture memory, but texels are undefined

	//Attach 2D texture to this FBO;
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex, 0);


	 // Depth to texture
	glGenTextures(1, &depthTex);
	glBindTexture(GL_TEXTURE_2D, depthTex);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32,
			bufferWidth, bufferHeight, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, fb);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthTex, 0);

	//Does the GPU support current FBO configuration?
	if( glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
		fprintf(stderr, "Cannot create Framebuffer\n");
		exit(EXIT_FAILURE);
	}

	// set up both attachments for drawing to
	GLuint attachments[2] = { GL_COLOR_ATTACHMENT0, GL_DEPTH_ATTACHMENT };
	glDrawBuffers(2,  attachments);


}

void initPPRectangle(){
	GLfloat fbo_vertices[] = {
		-1.0, -1.0,
		1.0, -1.0,
		-1.0,  1.0,
		1.0,  1.0,
	};

	glGenBuffers(1, &rectbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, rectbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(fbo_vertices), fbo_vertices, GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void updateFPSCounter(float dt){
	static char fps[255];
	static float nFrames = 0;
	static float t = 0.0f;
	nFrames++;
	t += dt;
	if( t >= 1.0) {
		sprintf(fps, "%s: %f fps", winTitle, nFrames);
		printf("%s: %f fps \n", winTitle, nFrames);
		glfwSetWindowTitle(fps);
		nFrames = 0.0f;
		t = 0.0f;
	}
}

void Cleanup() {
	glfwTerminate();
	glDeleteBuffers(1, &vertexbuffer);
	glDeleteBuffers(1, &rectbuffer);
	glDeleteTextures(1, &tex);
	glDeleteRenderbuffers(1, &depth_rb);

	//bind 0, i.e. back buffer, to ensure that fb is unbound
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glDeleteFramebuffers(1, &fb);

	cgDestroyContext(cgContext);
}

Mesh createPlaneMesh(int gridSizeX, int gridSizeZ, float gridDensityX, float gridDensityZ){
	// create vertices
	float numberOfCellsX = gridSizeX/gridDensityX;

	float numberOfCellsZ = gridSizeZ/gridDensityZ;
	int numberOfCells =  numberOfCellsX * numberOfCellsZ;
	int numberOfvertices = (numberOfCellsX + 1)  * (numberOfCellsZ + 1);

	GLfloat * vertices = new GLfloat[numberOfvertices * 3];

	int i = 0;
	for(float x = -gridSizeX/2.0; x<= gridSizeX/2.0; x+=gridDensityX){
		for(float z = -gridSizeZ/2.0; z <= gridSizeZ/2.0; z+=gridDensityZ){
			vertices[i] = x;
			vertices[i+1] = 0;
			vertices[i+2] = z;
			i += 3;
		}
	}

	// create indexes
	int numberOfIndexes = numberOfCells * 2 * 3;
	GLuint * indexes = new GLuint[numberOfIndexes];

	int numberOfVerticesZ = numberOfCellsZ + 1;

	int j = 0;
	for(int iX = 0; iX < numberOfCellsX; iX++){
		for(int iZ = 0; iZ < numberOfCellsZ; iZ++){
			indexes[j] = iX + numberOfVerticesZ * iZ;
			indexes[j+1] = iX + numberOfVerticesZ * iZ + 1;
			indexes[j+2] = iX + numberOfVerticesZ * (iZ + 1);

			indexes[j+3] = iX + numberOfVerticesZ * iZ + 1;
			indexes[j+4] = iX + numberOfVerticesZ * (iZ + 1);
			indexes[j+5] = iX + numberOfVerticesZ * (iZ + 1) + 1;

			j+= 6;
		}
	}
	return Mesh(vertices, numberOfvertices, indexes, numberOfIndexes);
}

Mesh createPyramidMesh(){
	GLfloat vertices[] = {
	 -1/2.0,  0.0f, -1/2.0,
	 1/2.0,  0.0f, -1/2.0,
	 -1/2.0,  0.0f, 1/2.0,
	 1/2.0,  0.0f, 1/2.0,
	 0.0f, 4.0f, 0.0f
	};

	GLuint indices[] = {
	   0,1,3,
	   0,2,3,
	   0,2,4,
	   0,1,4,
	   1,3,4,
	   2,3,4
	};

	GLfloat * verticesCopy = new GLfloat[15];
	memcpy(verticesCopy, vertices, 15 * sizeof(GLfloat));

	GLuint * indicesCopy = new GLuint[18];
	memcpy(indicesCopy, indices, 18* sizeof(GLuint));

	return Mesh(verticesCopy, 5, indicesCopy, 18);
}

void handleInput(Player * player){
	if(glfwGetKey( 'W' ) == GLFW_PRESS)
		player->advance(FORWARD);

	if(glfwGetKey( 'S' ) == GLFW_PRESS)
		player->advance(BACKWARDS);

	if(glfwGetKey( 'A' ) == GLFW_PRESS)
		player->advance(LEFT);

	if(glfwGetKey( 'D' ) == GLFW_PRESS)
		player->advance(RIGHT);

	if(glfwGetKey( GLFW_KEY_SPACE ) == GLFW_PRESS)
			player->jump();


	if(glfwGetKey('0') == GLFW_PRESS){
		setupBlurKernel(identitylKernel);
		motionBlurOn = false;
	}

	if(glfwGetKey('1') == GLFW_PRESS){
		setupBlurKernel(gaussianKernel);
		motionBlurOn = false;
	}

	if(glfwGetKey('2') == GLFW_PRESS){
		motionBlurOn = true;
		setupMotionBlurTrailLength(0.7);
	}

	// Extreme motion blur
	if(glfwGetKey('3') == GLFW_PRESS){
		motionBlurOn = true;
		setupMotionBlurTrailLength(3);
	}


	int x,y;

	glfwGetMousePos(&x, &y);

	if(!firstFrame){
		float angleX = (float)(previouseMouseX - x)/winWidth;
		float angleY = (float)(previouseMouseY - y)/winHeight;
		player->turn(angleX, angleY);
	}

	previouseMouseX = x;
		previouseMouseY = y;
	firstFrame = false;
}

void updateWorld(float dt, Player * player, Camera * camera) {
	player->step(dt);

	pWvp = wvp;
	mView = camera->getViewMatrix();
	wvp = mProjection * mView * mWorld;
	// pass matrix parameters to shader
	cgGLSetMatrixParameterfc(shaderWVP, value_ptr(wvp));
	cgGLSetMatrixParameterfc(shaderpWvp, value_ptr(pWvp));
	cgGLSetMatrixParameterfc(shaderiWvp, value_ptr(inverse(wvp)));
}

int main( int argc, char** argv ) {
	initGL(winTitle);
	initCg("postprocess.cgfx");
	setupBlurKernel(identitylKernel);
	bool postPorccess = true;

	if(postPorccess)
		initFramebuffer();

	initPPRectangle();

	Mesh plane = createPlaneMesh(30,30,1,1);
	GLDrawer planeDrawer(&plane, cgRenderVP);

	Mesh pyramid = createPyramidMesh();
	GLDrawer pyramidDrawer(&pyramid, cgRenderVP);

	PlayerPropeties playerProperties;
	playerProperties.speed = 20; // units per second
	playerProperties.jumpSpeed = 16;
	playerProperties.height = 1.5;
	playerProperties.angleSpeed = 20;

	Player * player = new Player(playerProperties);
	Camera * camera = new Camera(player);

	cgGLSetupSampler( shaderSampler, tex);
	cgGLSetupSampler( shaderDepthSampler, depthTex);

	double lastTime = glfwGetTime(), currentTime;

	do {
		currentTime = glfwGetTime();

		handleInput(player);

		updateFPSCounter(currentTime - lastTime);
		updateWorld(currentTime - lastTime, player, camera);

		lastTime = currentTime;

		// render to texture
		cgSetPassState(cgRenderPass);
		glBindFramebuffer(GL_FRAMEBUFFER, fb);

		glClearColor(0, 0, 0, 1.0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		planeDrawer.draw();
		pyramidDrawer.draw();

		if (postPorccess){
			if (!motionBlurOn){
				cgSetPassState(cgPostProcessPass);
				glBindFramebuffer(GL_FRAMEBUFFER, 0);

				glClearColor(0.0, 0.3, 0.0, 1.0);
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

				cgGLEnableTextureParameter(shaderTexture);

				glBindBuffer(GL_ARRAY_BUFFER, rectbuffer);

				cgGLEnableClientState(cgPostProcessPos);
				cgGLSetParameterPointer(cgPostProcessPos, 2, GL_FLOAT, 0, 0);

				glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

				cgGLDisableTextureParameter(shaderTexture);
				cgGLDisableClientState(cgPostProcessPos);
			}
			else{
				// post-process
				cgSetPassState(cgMotionBlurPass);

				glBindFramebuffer(GL_FRAMEBUFFER, 0);

				glClearColor(0.0, 0.3, 0.0, 1.0);
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


				cgGLEnableTextureParameter(shaderTexture);
				cgGLEnableTextureParameter(shaderDepthTexture);

				glBindBuffer(GL_ARRAY_BUFFER, rectbuffer);

				cgGLEnableClientState(cgPostProcessPos);
				cgGLSetParameterPointer(cgPostProcessPos, 2, GL_FLOAT, 0, 0);

				glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

				cgGLDisableTextureParameter(shaderTexture);
				cgGLDisableTextureParameter(shaderDepthTexture);
				cgGLDisableClientState(cgPostProcessPos);
			}
		}

		glfwSwapBuffers();

	} while( glfwGetKey( GLFW_KEY_ESC ) != GLFW_PRESS && glfwGetWindowParam( GLFW_OPENED ) );

	Cleanup();
	exit(EXIT_SUCCESS);
}

