/*
 * Mesh.h
 *
 *  Created on: Apr 13, 2013
 *      Author: alex
 */

#ifndef MESH_H_
#define MESH_H_

#include <vector>

#include <GL/glew.h>
#include <GL/glfw.h>

class Mesh {
	GLfloat * vertices;
	GLuint * indexes;
	int verticesSize;
	int indexesSize;
public:
	Mesh(GLfloat vertices[], int verticesSize, GLuint indexses[], int indexsesSize);
	virtual ~Mesh();

	GLuint* getIndexes();

	int getIndexesSize();

	int getVerticesSize();

	GLfloat* getVertices();
};

#endif /* MESH_H_ */
