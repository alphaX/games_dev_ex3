/*
 * player.hpp
 *
 *  Created on: Mar 9, 2013
 *      Author: alex
 */
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

#ifndef PLAYER_HPP_
#define PLAYER_HPP_

const float GRAVITY_FORCE = 0.5;

enum Direction {
	FORWARD,
	BACKWARDS,
	LEFT,
	RIGHT
} ;

typedef enum{
	NO_TURN,
	CLOCKWISE,
	ANTI_CLOCKWISE
} TurnDirection;

struct PlayerPropeties{
	float speed;
	float angleSpeed;
	float height;
	float jumpSpeed;
};

class Player{
	Direction direction;
	glm::vec3 turnDirection;
	glm::vec3 position;
	glm::vec3 currentSpeed;
	PlayerPropeties properties;


public:
	Player(PlayerPropeties properties);
	void advance(Direction direction);
	void turn(float turnX, float turnY);
	void jump();
	void step(float dt);
	glm::vec3 getPosition();
	glm::vec3 getCurrentTurn();
	float getHeight();
};


#endif /* PLAYER_HPP_ */
