/*
 * camera.cpp
 *
 *  Created on: Mar 9, 2013
 *      Author: alex
 */

#include "camera.hpp"
#define EPSILON 0.01


Camera::Camera(Player * player){
	this->player = player;
}

glm::mat4 Camera::getViewMatrix(){
	glm::mat4 viewMatrix(1);

	glm::vec3 position = this->player->getPosition();
	float height = this->player->getHeight();
	viewMatrix = glm::translate(-position.x, -(position.y + height), -position.z) * viewMatrix;
/*
	float cameraAngle = glm::angle(player->getCurrentTurn(), glm::vec3(0,0,-1));

	if(fabs(cameraAngle) > EPSILON && fabs(fabs(cameraAngle) - 180) > EPSILON){
		glm::vec3 axis = glm::cross(player->getCurrentTurn(), glm::vec3(0,0,-1));
		viewMatrix = glm::rotate(glm::mat4(), cameraAngle, axis) * viewMatrix;
	}*/
	glm::mat4 lookat = glm::lookAt(glm::vec3(0.0f,0.0f,0.0f), player->getCurrentTurn(), glm::vec3(0.0f,1.0f,0.0f));
	viewMatrix = lookat * viewMatrix;
	return viewMatrix;
}
