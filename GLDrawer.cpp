/*
 * GLDrawer.cpp
 *
 *  Created on: Apr 13, 2013
 *      Author: alex
 */

#include "GLDrawer.hpp"
#include <stdio.h>


GLDrawer::GLDrawer(Mesh* mesh, CGprogram cgProgram) {
	this->mesh = mesh;
	this->cgProgram = cgProgram;

	if(!(positionParameter = cgGetNamedParameter(cgProgram, "pos"))){
		fprintf(stderr, "Cannot get a handle to a Render VP parameter pos\n");
	}

	glGenBuffers(1, &this->vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, this->vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * mesh->getVerticesSize() * 3, mesh->getVertices(), GL_STATIC_DRAW);

	glGenBuffers(1, &this->indexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->indexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * mesh->getIndexesSize(), mesh->getIndexes(), GL_STATIC_DRAW);
}

void GLDrawer::draw() {
	glBindBuffer(GL_ARRAY_BUFFER, this->vertexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->indexBuffer);

	cgGLEnableClientState(this->positionParameter);
	cgGLSetParameterPointer(this->positionParameter, 3, GL_FLOAT, 0, 0);

	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glDrawElements(GL_TRIANGLES, this->mesh->getIndexesSize(), GL_UNSIGNED_INT, (GLvoid*)0);
	//glDrawArrays(GL_TRIANGLES, 0, vertices.size());

	cgGLDisableClientState(this->positionParameter);
}

GLDrawer::~GLDrawer() {
	glDeleteBuffers(1, &(this->vertexBuffer));
	glDeleteBuffers(1, &(this->indexBuffer));
}
