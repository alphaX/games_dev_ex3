/*
 * player.cpp
 *
 *  Created on: Mar 9, 2013
 *      Author: alex
 */

#include "player.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/rotate_vector.hpp>

Player::Player(PlayerPropeties properties){
	this->properties = properties;
	this->turnDirection = glm::vec3(0,0,-1);
	this->position = glm::vec3(0,0,0);
	this->currentSpeed = glm::vec3(0,0,0);
}

void Player::advance(Direction direction){
	this->direction = direction;
	float oldYSpeed = this->currentSpeed.y;
	switch(direction){
	case FORWARD:
		this->currentSpeed = this->turnDirection*this->properties.speed;
		break;
	case BACKWARDS:
		this->currentSpeed = -this->turnDirection*this->properties.speed;
		break;
	case LEFT:
		this->currentSpeed = glm::rotate(this->turnDirection, 90.0f, glm::vec3(0.0f, 1.0f, 0.0f))*this->properties.speed;
		break;
	case RIGHT:
		this->currentSpeed = glm::rotate(this->turnDirection, -90.0f, glm::vec3(0.0f, 1.0f, 0.0f))*this->properties.speed;
		break;
	}
	this->currentSpeed.y = oldYSpeed;
}

void Player::turn(float turnX, float turnY){
	//glm::vec3 axis = glm::cross(this->turnDirection, glm::vec3(0,1,0));
	glm::vec3 axisX = glm::vec3(0,1,0);
	glm::vec3 axisY = glm::cross(this->turnDirection, glm::vec3(0,1,0));
	this->turnDirection = glm::rotate(this->turnDirection, turnX*this->properties.angleSpeed, axisX);

	this->turnDirection = glm::rotate(this->turnDirection, turnY*this->properties.angleSpeed, axisY);
}

glm::vec3  Player::getPosition(){
	return this->position;
}


void Player::jump(){
	if(this->position.y == 0)
		this->currentSpeed.y = this->properties.jumpSpeed;
}

void Player::step(float dt){
	if(this->getPosition().y > 0)
		this->currentSpeed.y -= GRAVITY_FORCE;

	if(this->getPosition().y < 0){
		this->position.y = 0;
		this->currentSpeed.y = 0;
	}

	this->position += this->currentSpeed*dt;
	this->currentSpeed.x = 0;
	this->currentSpeed.z = 0;
}

float Player::getHeight(){
	return this->properties.height;
}

glm::vec3 Player::getCurrentTurn(){
	return this->turnDirection;
}
